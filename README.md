# modules_and_packages_2022_07_19_gr_14_07

---

[video](https://youtu.be/XN4BYTczVkU)


---


## Порядок формування списку каталогів для пошуку модулів:

- каталог, з якого був запущений сценарій (поточний каталог)
- список каталогів які містяться у змінній середовища PYTHONPATH
- залежний від встановлення список каталогів (під час встановлення Python)

```python
import sys
print(sys.path)

import re
print(re.__file__)
```

> import <module_name>
> 
> import <module_name_1>[, <module_name_2>, ...]


> from <module_name> import <name(s)>

> from <module_name> import * # всі, окрім тіх, що починаються з "_"

> from <module_name> import <name_1> as <alt_name_1>[, <name_n> as <alt_name_n>, ...]

> import <module_name> as <alt_name>

```python
try:
    import baz
    #

except ImportError:
    print("Module not found")
```
> dir()
> dir(obj)

 
> import pkg.mod1, pkg.mod2


 