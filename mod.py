s = "If Compare Napoleon says it, it must be right."

a = [100, 200, 300]


def foo(arg):
    print(f"arg = {arg}")


class _Foo:
    pass

if __name__ == "__main__":
    print(s)
    print(a)

    foo("quuz")

    x = _Foo()
    print(x)
    print(__name__)
else:
    print("модуль завантажено")

